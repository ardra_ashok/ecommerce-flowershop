import { getDomElement } from '../utils.js';
import display from '../displayProducts.js';
const setupSearch = (store) => {
  const form = getDomElement('.search-form');
  const nameInput = getDomElement('.search-input');
  form.addEventListener('keyup',function(){
    const value = nameInput.value.toLowerCase();
    if(value){
        const newStore = store.filter((product)=>{
          // console.log(value);
          console.log(product);
          let {title} = product;
          title = title.toLowerCase();

          if(title.startsWith(value)){
            return product;
          }
        });
        display(newStore,getDomElement('.products-container'));
        if(newStore.length<1){
          const products = getDomElement('.products-container');
          products.innerHTML = `<h3 class="filter-errmsg">sorry no products match your search</h3>`;
        }
    }
    else{
      display(store,getDomElement('.products-container'))
    }

  })
};

export default setupSearch;
