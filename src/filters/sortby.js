import { getDomElement } from '../utils.js';
import display from '../displayProducts.js';


const sortby = (store) =>{
  const sortby = getDomElement('.sortby');
  const productsTitle = getDomElement('.productsTitle');
  sortby.addEventListener('change',function(e){
    let newStore =[];
    const element = e.target.value;
    if(element === 'price-ascending')
    {

      newStore = store.sort((a,b) =>{
        return (a.price - b.price);
      });
      display(newStore,getDomElement('.products-container'));
      productsTitle.innerHTML = `Sorted by "price - low to high"`;


    }
    else if(element === 'price-descending'){
      newStore = store.sort((a,b) =>{
        return (b.price - a.price);
      });
      display(newStore,getDomElement('.products-container'));
      productsTitle.innerHTML = `Sorted by "price - high to low"`;

    }
    else if(element === 'rating'){

      newStore = store.sort((a,b) =>{
        return (a.rating - b.rating);
      });
      // console.log(newStore);
      display(newStore,getDomElement('.products-container'));
      productsTitle.innerHTML = `Products sorted by "rating"`;

    }
  });
}
  export default sortby;



