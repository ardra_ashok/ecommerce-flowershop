import { getDomElement } from '../utils.js';
import display from '../displayProducts.js';

const setupCompanies = (store) => {
  const companiesDOM = getDomElement('.companies');
  companiesDOM.addEventListener('change',function(e){
    const element = e.target.value;
    const productsTitle = getDomElement('.productsTitle');
   // console.log(element);

      let newStore = [];
      if(element === 'all'){
        newStore = [...store];
        productsTitle.innerHTML = `All products`;
      }
      else{

        newStore = store.filter((product)=>product.company === e.target.value);
        productsTitle.innerHTML = `Products Search for "${element}"`;

      }


      display(newStore,getDomElement('.products-container'));

  })
};
export default setupCompanies;
