// get icon bar links and search form
const els = document.querySelector( '.searchIcon' );
const searchForm = document.querySelector( '.search-form');
const searchFormInput = document.querySelector( '.search-form input');

els.addEventListener('click',searchActive);

function searchActive(){
  if ( searchForm.style.opacity == 0 ) {
    searchForm.style.opacity = 1;
  } else {
    searchForm.style.opacity = 0;
    searchFormInput.value = '';
  }
}