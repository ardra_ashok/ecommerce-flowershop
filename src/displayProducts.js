import { formatPrice } from './utils.js';
import { addToCart } from './cart/setupCart.js';
import { openCart } from './cart/toggleCart.js';



const display = (products,element) =>{

element.innerHTML = products.map((product)=>{
const { id, title, image, price } = product;
return ` <div class="col-4">
<div class="product-top">
<img src="${image}" alt="${title}">
<div class="overlay-right">

  <a href="product.html?id=${id}" class="btn-overlay side-buttons"><i class="fa fa-search"></i></a>
  <button type="button" class="btn-overlay side-buttons addtoCartBtn" title="Add to Cart" data-id="${id}">
    <i class="fa fa-shopping-cart"></i>
  </button>
</div>
</div>
<h4><a href="product-details.html">${title}</a></h4>
<p class="text-bold">${formatPrice(price)}</p>
</div>
`
}).join('');
  element.addEventListener('click', function(e){
    const parent = e.target.parentElement;

    if(parent.classList.contains('addtoCartBtn')){
     //console.log(parent.dataset.id);
     addToCart(parent.dataset.id);
     openCart();
    }
  });


}

export default display;