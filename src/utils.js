



// program for fetching the elements from the DOM
const getDomElement = (query) => {

  const element = document.querySelector(query);
  if (element)
   return element;
  throw new Error(`Please check your "${query}" selection , no such element exist`);
}


const formatPrice = (price) => {
  let formattedPrice = new Intl.NumberFormat('en-AU',{
    style:'currency',
    currency: 'AUD'
  }).format((price/100).toFixed(2));
  return formattedPrice;
}


const getStorageItem = (item) =>{
  let storageItem = localStorage.getItem(item);
  if(storageItem){
    storageItem = JSON.parse(localStorage.getItem(item));
  }
  else {
    storageItem = [];
  }
  return storageItem;
}
const setStorageItem = (name,item) =>{
  localStorage.setItem(name,JSON.stringify(item));
}


export {
  getDomElement,

  formatPrice,
  getStorageItem,
  setStorageItem,
}