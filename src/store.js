import { getStorageItem,setStorageItem } from "./utils.js";

let store = getStorageItem('store');
const setupStore = (products) =>{
  store = products.map((product)=>{
    const {id,fields:{company,title,newarrival,price,rating,image:img,desc}} = product;
    const image = img.fields.file.url;
    return {id,company,title,newarrival,price,rating,image,desc};
  });
  setStorageItem('store',store);
};

const findProductDetails = (id) =>{
  let product = store.find((product)=>product.id === id);
  return product;
};
export { store, setupStore, findProductDetails };

