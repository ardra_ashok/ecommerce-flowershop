
/* import './src/cart/toggleCart.js'; */
// global imports
import '../toggleNavbar.js';
import fetchProducts from '../fetchProducts.js'

// specific

import { getDomElement, formatPrice, getStorageItem, setStorageItem } from '../utils.js';
import { setupStore, store } from '../store.js';
/* import { openCart } from '../cart/toggleCart.js'; */


const loading = getDomElement('.page-loading');
const cartItemsDOM = getDomElement('.cartPage-items');
const cartPageDetails = getDomElement('.cart-page');
const cartTotalDOM = getDomElement('.cartPage-total');
const cartItemCountDOM = getDomElement('.cart-item-count');
const checkoutBtn = getDomElement('.checkoutBtn');


let cart = getStorageItem('cart');


// display cart Total
 function displayCartTotal(){

  let total = cart.reduce((total,cartItem)=>{
    return ( total += cartItem.price * cartItem.qty);
  }, 0);
   cartTotalDOM.textContent = `Total: ${formatPrice(total)}`;
   checkoutBtn.innerText = `Pay ${formatPrice(total)}`;
   //let totalPrice = formatPrice(total);
   
 // return totalPrice;
  
}


function displayCartItemCount(){
  const amount = cart.reduce((total,cartItem)=>{
    return (total += cartItem.qty);
  },0);
  cartItemCountDOM.textContent = amount;
  
 // cartItemCount.textContent = `View Cart (${amount})`;
}


  function setupCartFunctionality(){
    cartItemsDOM.addEventListener('click',function(e){
      const element = e.target;


      const parent = e.target.parentElement;
      const id = e.target.dataset.id;
     // const parentID = e.target.parentElement.dataset.id;

      // remove an item from the cart
      if(element.classList.contains('cart-item-remove-btn')){
        removeItem(id);
       parent.parentElement.parentElement.parentElement.remove();
      }
      if(element.classList.contains('quantity')){

        element.addEventListener('input', function(e){
          const numberErrorInput = [...cartItemsDOM.querySelectorAll('.numberError')];
          const numberErrorInputDom =  numberErrorInput.find((value)=>value.dataset.id === id);
          numberErrorInputDom.style.display = 'none';
          let inputValue = parseInt(element.value);
          if (isNaN(inputValue)) {

            //console.log(numberErrorInput);
            //console.log(numberErrorInputDom);
            numberErrorInputDom.style.display = 'block';
          }

          else if(inputValue === 0){
            numberErrorInputDom.style.display = 'none';
            removeItem(e.target.parentElement.parentElement.dataset.id);
            e.target.parentElement.parentElement.remove();
            if(cart.length === 0){
                getDomElement('.cart-page').innerHTML = `<div class="cart-empty">
                <h3 class="error">No Items in your cart. please add Something</h3>
                <a href="./products.html" class="btn">Back to Shop</a>
              </div>`;
            }
          }
          else{
          numberErrorInputDom.style.display = 'none';

            const nowInput = changeCount(id,inputValue);
            //console.log(nowInput);
            const items = [...cartItemsDOM.querySelectorAll('.totalPriceItem')];
            //console.log(items);
            const newPriceTotalDom =  items.find((value)=>value.dataset.id === id) //this is the DOM to display the price
            //console.log(newPriceTotalDom);
            const priceListDom = [...cartItemsDOM.querySelectorAll('.price')];
           /*  const singlePriceDom = priceListDom.find((value)=>value.dataset.id===id);
            console.log(singlePriceDom.textContent); */
            const cartPriceItem = cart.find((cartItem)=>cartItem.id === id);
            newPriceTotalDom.textContent = `${formatPrice(cartPriceItem.price * nowInput)}`;
          }
        })
      }
      displayCartItemCount();
      displayCartTotal();
      setStorageItem('cart',cart);
    });

    }

    function changeCount(id,qty){
      let newAmount;
      cart = cart.map((cartItem)=>{
        if(cartItem.id === id){
            newAmount =  qty;
            cartItem = {...cartItem,qty: newAmount}
        }
        return cartItem;

      })
      return newAmount;
    }



function removeItem(id){
  cart = cart.filter((cartItem)=> cartItem.id !== id);
  if (cart.length === 0) {
    loading.style.display = 'none';
    cartPageDetails.innerHTML = `<div class="cart-empty">
    <h3 class="error">No Items in your cart. please add Something</h3>
    <a href="./products.html" class="btn">Back to Shop</a>
  </div>`;
  }

  //console.log(cart);
  setStorageItem('cart',cart);
  displayCartTotal();
  displayCartItemCount();
}
 
const init =()=>{
  if(cart.length === 0){
    loading.style.display = 'none';
    cartPageDetails.innerHTML = `<div class="cart-empty">
    <h3 class="error">No Items in your cart. please add Something</h3>
    <a href="./products.html" class="btn">Back to Shop</a>
  </div>`;

  }
  else{
  loading.style.display = 'none';


  cartItemsDOM.innerHTML = cart.map((item)=>{
    const { id, title, image, price,qty } = item;

      return `<tr class="cartPage-item" data-id="${id}" >
      <td>
          <div class="cart-info">
              <img src="${image}" alt="${title}">
              <div>
                  <p>${title}</p>
                  <small class="price" data-id="${id}">Price: ${formatPrice(price)}</small><br>
                  <a class="cart-item-remove-btn" data-id="${id}">Remove</a>
              </div>
          </div>
      </td>
      <td>
        <input type="number" class="quantity" data-id="${id}" value="${qty}" min="0">
        <p class="numberError" data-id="${id}"><i class="fas fa-exclamation-circle"></i>Quantity must be 1 or more</p>
      </td>

      <td class="totalPriceItem" data-id="${id}"> ${formatPrice(item.price*item.qty)}</td>

  </tr>`
  }).join('');
  
  }

  /* function checkout() {
    // console.log(cart);
    let paypalFormHTML = `
    <form id="paypal-form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
      <input type="hidden" name="cmd" value="_cart">
      <input type="hidden" name="upload" value="1">
      <input type="hidden" name="business" value="myemail@gmail.com">
  `;
    cart.forEach((cartItem, index) => {
    //console.log(cartItem.title);
    // console.log(formatPrice(cartItem.price));
    ++index;
    paypalFormHTML += `
      <input type="hidden" name="item_name_${index}" value="${cartItem.title}">
      <input type="hidden" name="amount_${index}" value="${formatPrice(cartItem.price)}">
      <input type="hidden" name="quantity_${index}" value="${cartItem.qty}">
    `;
  });   
   paypalFormHTML += `
      <input type="submit" value="PayPal">
    </form>
    <div class="overlay"></div>
  `;
  document.querySelector('body').insertAdjacentHTML('beforeend', paypalFormHTML);
  document.getElementById('paypal-form').submit();
  }

  checkoutBtn.addEventListener('click', () => checkout()); */
    // display amount of cart items
    displayCartItemCount();
    // display total
    displayCartTotal();
    //add all cart items to the dom
    displayCartItemCount();
  //setup cart functionality
 
  setupCartFunctionality();

  

};
init();




