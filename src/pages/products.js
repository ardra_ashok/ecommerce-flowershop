
// global imports
import '../toggleNavbar.js';
import '../toggleSearchbar.js';
import '../cart/setupCart.js';
/* import '../cart/toggleCart.js'; */




//  filter imports
import setupSearch from '../filters/search.js';
import setupCompanies from '../filters/companies.js';
import sortBy from '../filters/sortby.js';



// specific imports
import { store } from '../store.js';
import display from '../displayProducts.js';
import { getDomElement } from '../utils.js';
// import { addToCart } from '../cart/setupCart.js';

const loading = getDomElement('.page-loading');


const init =()=>{

  display(store,getDomElement('.products-container'));



};
init();


setupSearch(store);
setupCompanies(store);
sortBy(store);


loading.style.display = 'none';
