
 import '../toggleNavbar.js';

import { getDomElement,formatPrice,getStorageItem, setStorageItem } from '../utils.js';
/* import { openCart } from './toggleCart.js'; */


import { findProductDetails } from '../store.js';
 import  addToCartDOM from './addToCartDOM.js';


// set items
  const cartItemCountDOM = getDomElement('.cart-item-count');

  const cartItemCount = getDomElement('.cartRefBtn');
  let cart = getStorageItem('cart');



 export const addToCart = (id,qty=1) => {




   let item = cart.find((cartItem)=>cartItem.id === id);




 // console.log(item);
   if(!item){
    let product = findProductDetails(id);


    // console.log(product);
    product = { ...product, qty };
    cart = [ ...cart, product ];
    // console.log(cart);
    addToCartDOM(product);


  }
  else{
    //update values
   //
   let product = findProductDetails(id);
   product = { ...product, qty };
   addToCartDOM(product);
    const count = increaseCount(id,qty);
    //console.log(count)





}
  //add one item to the item count */

   displayCartItemCount();

  setStorageItem('cart',cart);
  //openCart();


 }


function displayCartItemCount(){
  const amount = cart.reduce((total,cartItem)=>{
    return (total += cartItem.qty);
  },0);
  cartItemCountDOM.textContent = amount;

cartItemCount.textContent = `View Cart (${amount})`;
}

function increaseCount(id,qty){
  let newAmount;
  cart = cart.map((cartItem)=>{
    if(cartItem.id === id){
        newAmount=cartItem.qty + qty;
        cartItem = {...cartItem,qty: newAmount}
    }
    return cartItem;
  })
  return newAmount;
}


const init =()=>{
  //display amount of cart Items
  displayCartItemCount();

};

init();



