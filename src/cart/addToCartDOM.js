import { getDomElement,formatPrice } from '../utils.js';
import { addToCart } from './setupCart.js';

const cartItemsDOM = getDomElement('.cart-items');

 const addToCartDOM = ({id,title,price,image,qty}) => {
 // console.log(formatPrice(price));
  cartItemsDOM.innerHTML = `<div class="cart-item">
        <img src="${image}" alt="${title}" class="cart-item-img">
        <div>
          <h4 class="cart-item-name">${title}</h4>
          <p class="cart-item-price">${formatPrice(price)}</p>
        </div>
        <div>
          <p class="cart-item-amount" data-id="${id}">Qty:${qty}</p>

        </div>
  </div>`;


}
/* const addToCartDOM = ({id,title,price,image,qty})=>{


}

/* const addToCartDOM = ({id,title,price,image,qty})=>{
  console.log(cartItemsDOM);
  //console.log(title);
  const cartPageItemDiv = document.createElement('div');
  cartPageItemDiv.classList.add('cart-item');
  cartPageItemDiv.innerHTML = ` <img src="${image}" alt="${title}" class="cart-item-img">
  <div>
    <h4 class="cart-item-name">${title}</h4>
    <p class="cart-item-price">${formatPrice(price)}</p>
  </div>
  <div>
    <p class="cart-item-amount" data-id="${id}">Qty:${qty}</p>

  </div>`;
  cartItemsDOM.appendChild(cartPageItemDiv);

} */

export default addToCartDOM;

